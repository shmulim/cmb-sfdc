List<Commission__c> commissions = [SELECT Id FROM Commission__c];
delete commissions;

List<Opportunity> opportunities = [SELECT Id FROM Opportunity];
delete opportunities;

List<Sales_Agent__c> salesAgents = [SELECT Id FROM Sales_Agent__c];
delete salesAgents;