trigger OpportunityTrigger on Opportunity (after insert, after update, before delete) {

    OpportunityTriggerHandler oppTriggerHandler = new OpportunityTriggerHandler();

    // needs record Ids so the `after insert` trigger is used
    if ((Trigger.isInsert || Trigger.isUpdate) && Trigger.isAfter) {
        oppTriggerHandler.createCommissions(Trigger.new);
    }

    if (Trigger.isBefore && Trigger.isDelete) {
        oppTriggerHandler.destroyCommissions(Trigger.old);
    }

}