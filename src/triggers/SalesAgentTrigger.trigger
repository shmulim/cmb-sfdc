trigger SalesAgentTrigger on Sales_Agent__c (before delete) {

    if (Trigger.isBefore && Trigger.isDelete) {
        SalesAgentTriggerHandler salesAgentTriggerHandler = new SalesAgentTriggerHandler();
        salesAgentTriggerHandler.destroyCommissions(Trigger.old);
    }

}