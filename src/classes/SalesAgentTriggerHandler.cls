public with sharing class SalesAgentTriggerHandler {

    /**
    * Deletes commission records when their parent sales agent is deleted.
    * @param List<Sales_Agent__c> salesAgents (Trigger.old)
    * @return null
    */
    public void destroyCommissions(List<Sales_Agent__c> salesAgents) {
        List<Id> agentIds = new List<Id>();
        for (Sales_Agent__c salesAgent : SalesAgents) {
            agentIds.add(salesAgent.Id);
        }

        List<Commission__c> commissions = [
            SELECT Id FROM Commission__c
            WHERE Sales_Agent__c IN :agentIds
        ];

        delete commissions;
    }

}