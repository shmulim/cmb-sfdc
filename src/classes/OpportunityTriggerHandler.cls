public with sharing class OpportunityTriggerHandler {

    private Decimal commissionRate = 10;
    private List<Opportunity> opportunities;
    private List<Id> oppOwnerIds;
    private List<String> history = new List<String>();

    /**
    * Creates/updates commission and sales agent records when an opportunity with StageName
    * is created with values `New`, `Pending, or `Completed`. Tracks history and performs a check
    * to ensure a bulk operation won't create duplicate sales agents. Utilizes UnitofWork for a single
    * atomic commit or rollback and for DML bulkification. UoW registered sales agents are cached
    * in a map in order to correctly relate them to commission records. If the sales agent record
    * exists, the commission record is bound to that existing sales agent. Else a new record is used.
    * @param List<Opportunity> opportunities (Trigger.old)
    * @return null
    */
    public void createCommissions(List<Opportunity> opportunities) {
        this.opportunities = opportunities;
        buildOppOwnerIdList();

        Map<Id, User> oppOwnerMap = buildOppOwnerMap();
        Map<Id, Decimal> totalsMap = buildTotalsMap();
        Map<String, Sales_Agent__c> agentMap = buildAgentMap();
        Map<String, Sales_Agent__c> uowMap = new Map<String, Sales_Agent__c>();

        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new List<Schema.SObjectType>{Sales_Agent__c.SObjectType, Commission__c.SObjectType}
        );

        for (Opportunity opp : opportunities) {

            if (opp.StageName == 'New' || opp.StageName == 'Pending' || opp.StageName == 'Completed') {

                Sales_Agent__c salesAgent = (agentMap.get(opp.OwnerId) != null)
                    ? agentMap.get(opp.OwnerId)
                    : new Sales_Agent__c();

                if (history.contains(opp.OwnerId) == false) {
                   history.add(opp.OwnerId);
                   String oppOwner = oppOwnerMap.get(opp.OwnerId).Name;
                   Decimal sum = (Decimal)totalsMap.get(opp.OwnerId);

                   salesAgent.Name = oppOwner;
                   salesAgent.OwnerId = opp.OwnerId;
                   salesAgent.Amount_Pending__c = (commissionRate / 100) * sum;
                   uow.registerUpsert(salesAgent);
                   uowMap.put(opp.OwnerId, salesAgent);
                }

                salesAgent = uowMap.get(opp.OwnerId);
                Commission__c commission = new Commission__c();
                commission.Name = oppOwnerMap.get(opp.OwnerId).Name;
                commission.Opportunity__c = opp.Id;
                uow.registerNew(commission, Commission__c.Sales_Agent__c, salesAgent);
            }
        }
        uow.commitWork();
    }

    /**
    * Deletes commission records when their parent opportunity is deleted.
    * @param List<Opportunity> opportunities (Trigger.old)
    * @return null
    */
    public void destroyCommissions(List<Opportunity> opportunities) {
        List<Id> oppIds = new List<Id>();
        for (Opportunity opp : opportunities) {
            oppIds.add(opp.Id);
        }

        List<Commission__c> commissions = [
            SELECT Id FROM Commission__c
            WHERE Opportunity__c IN :oppIds
        ];

        delete commissions;
    }

    /**
    * Builds a map with opportunity sums, indexed by opportunity ownerId.
    * Used for lookup when calculating commission and assigning value to salesAgent.Amount_Pending__c.
    * @return Map<Id, Decimal>
    */
    private Map<Id, Decimal> buildTotalsMap() {
        List<AggregateResult> sumResults = [
            SELECT OwnerId, SUM(Amount) total
            FROM Opportunity
            WHERE OwnerId IN :oppOwnerIds
            GROUP BY OwnerId
        ];

        Map<Id, Decimal> totalsMap = new Map<Id, Decimal>();
        for (AggregateResult result : sumResults) {
            Id ownerId = (Id)result.get('OwnerId');
            Decimal total = (Decimal)result.get('total');
            totalsMap.put(ownerId, total);
        }

        return totalsMap;
    }

   /**
   * Builds an opportunity owner map. Used for assigning salesAgent.Name based on
   * opportunity owner. Required since ths opportunity object does not have an ownerName field.
   * @return Map<Id, User>
   */
    private Map<Id, User> buildOppOwnerMap() {
        return new Map<Id, User>([
            SELECT Id, Name
            FROM User
            WHERE Id IN :oppOwnerIds
        ]);
    }

    /**
    * Builds a sales agent map to verify if the sales agent already exists. Used to determine
    * if a new sales agent record should be created or bind updates to an existing record.
    * @return Map<String, Sales_Agent__c>
    */
    private Map<String, Sales_Agent__c> buildAgentMap() {
        List<Sales_Agent__c> salesAgents = [
            SELECT Id, OwnerId
            FROM Sales_Agent__c
            WHERE OwnerId IN :OppOwnerIds
        ];

        Map<String, Sales_Agent__c> agentMap = new Map<String, Sales_Agent__c>();
        for (Sales_Agent__c agent : salesAgents) {
            agentMap.put(agent.OwnerId, agent);
        }
        return agentMap;
    }

    /**
    * Builds a list containing the trigger's opportunity ownerIds.
    * Assigns list to an instance variable used in map building methods.
    * @return null
    */
    private void buildOppOwnerIdList() {
        List<Id> oppOwnerIds = new List<Id>();
        for (Opportunity opp : opportunities) {
            oppOwnerIds.add(opp.OwnerId);
        }
        this.oppOwnerIds = oppOwnerIds;
    }

}